import { NgModule } from "@angular/core";
import { StoreModule } from "@ngrx/store";
// import { StoreDevtoolsModule } from "@ngrx/store-devtools";

import { environment } from "../../environments/environment";
import { reducerProvider, reducerToken } from "./app.reducer";

// import AppStoreModule in the AppModule after router module
@NgModule({
  imports: [
    StoreModule.forRoot(reducerToken, {
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true,
      },
    }),
    !environment.production ? [] : [],
  ],

  exports: [StoreModule],
  providers: [reducerProvider],
})
export class AppStoreModule {}
