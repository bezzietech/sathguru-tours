import { combineReducers, ActionReducerMap } from "@ngrx/store";
import { InjectionToken } from "@angular/core";
import { AuthState, authReducer } from "../auth/+state/reducers";
import {
  CustomerProfileState,
  customerProfileReducer
} from "../customer/customer-profile/+state/reducers";
import {
  BlogPostsState,
  blogPostsReducer
} from "../customer/customer-feeds/+state/reducers";
import { ToursState } from "../customer/customer-tours/+state/reducers";
export interface AppState {
  authState: AuthState;
  customerProfileState: CustomerProfileState;
  blogPostsState: BlogPostsState;
  toursState: ToursState;
}
export const reducerToken = new InjectionToken<ActionReducerMap<AppState>>(
  "Reducers"
);
export const reducers = combineReducers({
  authState: authReducer,
  customerProfileState: customerProfileReducer,
  blogPostsState: blogPostsReducer
});
export function getReducers() {
  return reducers;
}

export const reducerProvider = [
  { provide: reducerToken, useFactory: getReducers }
];
