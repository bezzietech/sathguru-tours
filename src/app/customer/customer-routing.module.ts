import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { CustomerProfileComponent } from "./customer-profile/components/registration/customer-profile.component";
import { CustomerFeedsComponent } from './customer-feeds/pages/load-feeds/customer-feeds.component';
import { CreateFeedComponent } from './customer-feeds/pages/create-feed/create-feed.component';
import { ProfilePageComponent } from './customer-profile/pages/profile-page/profile-page.component';

const routes: Routes = [
  {
    path: 'user-profile',
    component: ProfilePageComponent,
    pathMatch: 'full'
  },
  {
    path: 'sai-feeds',
    component: CustomerFeedsComponent,
    pathMatch: 'full'
  },
  {
    path: 'create-feed',
    component: CreateFeedComponent,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRoutingModule { }
