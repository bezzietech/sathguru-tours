import { Injectable } from "@angular/core";
import { SnackbarService } from "src/common/services";
import { State } from "@ngrx/store";
import { AppState } from "src/app/store/app.reducer";
import { createEffect, Actions, ofType } from "@ngrx/effects";
import * as TourActionTypes from "../actions";
import { of } from "rxjs";

import { switchMap, tap, map, catchError } from "rxjs/operators";
import { CustomerService } from "src/app/customer/customer.service";
import { ITour } from "src/interfaces";
@Injectable()
export class TourEffects {
  constructor(
    private snackbar: SnackbarService,
    private actions: Actions,
    private state: State<AppState>,
    private customerService: CustomerService
  ) {}
  tour_key: string = "";

  fetchTours = createEffect(() => {
    return this.actions.pipe(
      ofType(TourActionTypes.fetchTours),
      switchMap(() => {
        return this.customerService.fetchTours().pipe(
          map((tours: ITour[]) => TourActionTypes.fetchToursSuccess({ tours })),
          catchError(err => {
            this.snackbar.showSnackbar(
              "Sorry unable to fetch tours. Check your internet connection"
            );
            return of(TourActionTypes.fetchToursFailure(err));
          })
        );
      })
    );
  });

  enrollForTour = createEffect(() => {
    return this.actions.pipe(
      ofType(TourActionTypes.enrollForTour),
      switchMap(({ tourObject }) => {
        this.tour_key = tourObject.key;
        return this.customerService.enrollForTour(tourObject).pipe(
          map((tour: ITour) => {
            this.snackbar.showSnackbar("Enrolled Succesfully.");
            return TourActionTypes.enrollForTourSuccess({
              tour,
              key: this.tour_key
            });
          }),
          catchError(err => {
            this.snackbar.showSnackbar(
              "Sorry unable to enroll for tour. Check your internet connection"
            );
            return of(TourActionTypes.enrollForTourFailure(err));
          })
        );
      })
    );
  });

  cancelEnrollment = createEffect(() => {
    return this.actions.pipe(
      ofType(TourActionTypes.cancelEnrollmentForTour),
      switchMap(({ tour_object }) => {
        this.tour_key = tour_object.key;
        return this.customerService.cancelEnrollment(tour_object).pipe(
          map((tour: ITour) => {
            this.snackbar.showSnackbar("cancelled Enrollment Succesfully.");
            return TourActionTypes.cancelEnrollmentForTourSuccess({
              tour,
              key: this.tour_key
            });
          }),
          catchError(err => {
            this.snackbar.showSnackbar(
              "Sorry unable to process request currently. Check your internet connection"
            );
            return of(TourActionTypes.cancelEnrollmentForTourFailure(err));
          })
        );
      })
    );
  });
}
