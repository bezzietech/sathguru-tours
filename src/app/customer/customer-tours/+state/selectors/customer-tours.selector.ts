import { AppState } from "src/app/store/app.reducer";
import { createSelector } from "@ngrx/store";
import { ToursState } from "../reducers";

const getToursState = (state: AppState) => state.toursState;

const getTours = createSelector(
  getToursState,
  (state: ToursState) => state.tours
);
export const getToursQuery = {
  getTours
};
