import { createAction, props } from "@ngrx/store";
import { ITour } from "src/interfaces";

export const fetchTours = createAction("[TOUR] FETCH TOURS", props<{}>());
export const fetchToursSuccess = createAction(
  "[TOUR] FETCH TOURS SUCCESS",
  props<{ tours: ITour[] }>()
);
export const fetchToursFailure = createAction(
  "[TOUR] FETCH TOURS FAILURE",
  props<{ err: any }>()
);

export const enrollForTour = createAction(
  "[TOUR] ENROLL FOR TOUR",
  props<{ tourObject: any }>()
);

export const enrollForTourSuccess = createAction(
  "[TOUR] ENROLL FOR TOUR SUCCESS",
  props<{ tour: ITour; key: string }>()
);

export const enrollForTourFailure = createAction(
  "[TOUR] ENROLL FOR TOUR FAILURE",
  props<{ err: any }>()
);

export const cancelEnrollmentForTour = createAction(
  "[TOUR] CANCEL ENROLLMENT",
  props<{ tour_object: any }>()
);

export const cancelEnrollmentForTourSuccess = createAction(
  "[TOUR] CANCEL ENROLLMENT SUCCESS",
  props<{ tour: ITour; key: string }>()
);

export const cancelEnrollmentForTourFailure = createAction(
  "[TOUR] CANCEL ENROLLMENT FAILURE",
  props<{ err: any }>()
);
