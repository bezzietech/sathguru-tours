import { ITour } from "src/interfaces";
import { createReducer, on } from "@ngrx/store";
export const TOURS_FEATURE_KEY = "POSTS";
import * as TourActionTypes from "../actions";
import * as _ from "lodash";
export interface ToursState {
  tours: ITour[];
}
export const toursInitialState: ToursState = {
  tours: []
};
export interface ToursPartialState {
  readonly [TOURS_FEATURE_KEY]: ToursState;
}

export const toursReducer = createReducer(
  toursInitialState,

  on(TourActionTypes.fetchToursSuccess, (state, { tours }) => ({
    ...state,
    tours
  })),
  on(TourActionTypes.enrollForTourSuccess, (state, { tour, key }) => {
    const cloned_state = _.cloneDeep(state);
    cloned_state.tours[key] = tour;
    return cloned_state;
  }),
  on(TourActionTypes.cancelEnrollmentForTourSuccess, (state, { tour, key }) => {
    const cloned_state = _.cloneDeep(state);
    cloned_state.tours[key] = tour;
    return cloned_state;
  })
);
export function toursReducers(state, action) {
  return toursReducer(state, action);
}
