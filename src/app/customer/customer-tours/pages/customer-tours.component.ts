import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/app.reducer';
import * as TourActionTypes from '../+state/actions';
import { Observable } from 'rxjs';
import { getToursQuery } from '../+state/selectors';
import * as jwtDecode from 'jwt-decode';
import { ITour } from 'src/interfaces';
import { MatDialog } from '@angular/material';
import { DoubtsModalComponent } from '../components/doubts-modal/doubts-modal.component';

@Component({
  selector: 'app-customer-tours',
  templateUrl: './customer-tours.component.html',
  styleUrls: ['./customer-tours.component.scss']
})
export class CustomerToursComponent implements OnInit {
  constructor(private store: Store<AppState>, public dialog: MatDialog) { }
  uid: string = jwtDecode(localStorage.getItem('access-token')).user_id;
  tour$: Observable<any>;

  month_names = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec'
  ];
  ngOnInit() {
    this.store.dispatch(TourActionTypes.fetchTours({}));
    this.tour$ = this.store.select(getToursQuery.getTours);
  }
  checkIfEnrolled = (tour: ITour) => {
    if (tour.enrolledUsers) {
      return tour.enrolledUsers.includes(this.uid);
    }
    return false;
  }
  getDate = (dateString: string, query: string) => {
    const formattedDateString =
      '' +
      dateString.split('-')[1] +
      '-' +
      dateString.split('-')[0] +
      '-' +
      dateString.split('-')[2];
    switch (query) {
      case 'date':
        return dateString.split('-')[0];
      case 'month':
        return this.month_names[parseInt(dateString.split('-')[1]) - 1];
      default:
        return dateString.split('-')[2];
    }
  }
  openDoubts(data) {
    this.dialog.open(DoubtsModalComponent, {
      height: '500',
      width: '80%',
      panelClass: 'custom-dialog-container',
      data: {
        dataKey: data
        // image_as_url: this.urlArray
      }
    });
  }

  onEnrollButtonClicked(tourObject: any) {
    this.store.dispatch(TourActionTypes.enrollForTour({ tourObject }));
  }

  cancelEnrollmentButtonClicked(tour_object: any) {
    this.store.dispatch(
      TourActionTypes.cancelEnrollmentForTour({ tour_object })
    );
  }
}
