import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerToursComponent } from './customer-tours.component';

describe('CustomerToursComponent', () => {
  let component: CustomerToursComponent;
  let fixture: ComponentFixture<CustomerToursComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerToursComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerToursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
