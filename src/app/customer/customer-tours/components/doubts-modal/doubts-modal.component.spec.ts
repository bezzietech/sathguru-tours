import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoubtsModalComponent } from './doubts-modal.component';

describe('DoubtsModalComponent', () => {
  let component: DoubtsModalComponent;
  let fixture: ComponentFixture<DoubtsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoubtsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoubtsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
