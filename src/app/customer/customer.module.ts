import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { storeFreeze } from "ngrx-store-freeze";

import { CustomerRoutingModule } from "./customer-routing.module";
import { CustomerProfileComponent } from "./customer-profile/components/registration/customer-profile.component";
import { UiComponentsModule } from "src/common/ui-components/src/lib/components/ui-components.module";
import {
  MatCardModule,
  MatButtonModule,
  MatToolbarModule,
  MatStepperModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatIconModule,
  MAT_DIALOG_DATA,
  MatDialogRef,
  MatTooltipModule,
  MatListModule,
} from "@angular/material";
import { ReactiveFormsModule } from "@angular/forms";
import { STEPPER_GLOBAL_OPTIONS } from "@angular/cdk/stepper";
import { EffectsModule } from "@ngrx/effects";
import { CustomerProfileEffects } from "./customer-profile/+state/effects";
import { environment } from "src/environments/environment";
// import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import {
  customerProfileReducer,
  CustomerProfileInitialState,
  CUSTOMER_FEATURE_KEY,
  customerProfileReducers,
} from "./customer-profile/+state/reducers";
import { StoreModule, Store } from "@ngrx/store";
import { CustomerFeedsComponent } from "./customer-feeds/pages/load-feeds/customer-feeds.component";
import { BlogPostEffects } from "./customer-feeds/+state/effects";
import {
  blogPostInitialState,
  blogPostsReducer,
  blogPostsReducers,
} from "./customer-feeds/+state/reducers";

import { CustomerService } from "./customer.service";
import { CreateFeedComponent } from "./customer-feeds/pages/create-feed/create-feed.component";
import { CustomerToursComponent } from "./customer-tours/pages/customer-tours.component";
import { TourEffects } from "./customer-tours/+state/effects";
import {
  toursReducer,
  toursInitialState,
  toursReducers,
} from "./customer-tours/+state/reducers";
import { DetailsComponent } from "./customer-profile/components/details/details.component";
import { ProfilePageComponent } from "./customer-profile/pages/profile-page/profile-page.component";
import { AppState } from "../store/app.reducer";
import { SideNavBarComponent } from "./customer-feeds/pages/side-nav-bar/side-nav-bar.component";

@NgModule({
  declarations: [
    CustomerProfileComponent,
    CustomerFeedsComponent,
    CreateFeedComponent,
    CustomerToursComponent,
    DetailsComponent,
    ProfilePageComponent,
    SideNavBarComponent,
  ],
  imports: [
    CommonModule,
    CustomerRoutingModule,
    UiComponentsModule,
    MatTooltipModule,
    MatCardModule,
    MatButtonModule,
    MatToolbarModule,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatIconModule,
    MatListModule,
    StoreModule.forFeature("customerProfileState", customerProfileReducers, {
      metaReducers: !environment.production ? [storeFreeze] : [],
    }),
    EffectsModule.forFeature([
      CustomerProfileEffects,
      BlogPostEffects,
      TourEffects,
    ]),
    !environment.production ? [] : [],
    StoreModule.forFeature("blogPostsState", blogPostsReducers, {
      metaReducers: !environment.production ? [storeFreeze] : [],
    }),
    StoreModule.forFeature("toursState", toursReducers, {
      metaReducers: !environment.production ? [storeFreeze] : [],
    }),
  ],
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { showError: true },
    },
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} },
    CustomerService,
  ],
})
export class CustomerModule {
  constructor(public store: Store<AppState>) {}
}
