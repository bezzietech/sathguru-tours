import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { CustomerProfileEffects } from './customer-profile.effects';

describe('CustomerProfileEffects', () => {
  let actions$: Observable<any>;
  let effects: CustomerProfileEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CustomerProfileEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get<CustomerProfileEffects>(CustomerProfileEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
