import { Injectable } from "@angular/core";
import { Actions, ofType, createEffect } from "@ngrx/effects";
import { SnackbarService } from "src/common/services";
import { Router } from "@angular/router";
import * as CustomerProfileActions from "../actions";
import { switchMap, map, catchError, tap } from "rxjs/operators";
import { of } from "rxjs";
import { CustomerService } from "src/app/customer/customer.service";

@Injectable()
export class CustomerProfileEffects {
  constructor(
    public snackbar: SnackbarService,
    public customerService: CustomerService,
    public actions: Actions,
    public router: Router
  ) {}

  logAdd = createEffect(() => {
    return this.actions.pipe(
      ofType(CustomerProfileActions.createCustomer),
      switchMap(({ newCustomer }) => {
        return this.customerService.addCustomer(newCustomer).pipe(
          tap(res => {
            this.snackbar.showSnackbar(
              `Hi! ${newCustomer.user_name}. Your deatils are stored succesfully`
            );
          }),
          map(newCustomer =>
            CustomerProfileActions.createCustomerSuccess({ newCustomer })
          ),
          catchError(error => {
            this.snackbar.showSnackbar("Error : Unable to Add User.");
            return of(CustomerProfileActions.createCustomerFailure({ error }));
          })
        );
      })
    );
  });

  fetchCustomerDetails = createEffect(() => {
    return this.actions.pipe(
      ofType(CustomerProfileActions.fetchCustomerDetails),
      switchMap(({ uid }) => {
        return this.customerService.fetchUserDetails(uid).pipe(
          tap(res => {}),
          map(customer => {
            console.log(customer);
            if (customer) {
              return CustomerProfileActions.fetchCustomerDetailsSuccess({
                customer
              });
            } else {
              this.snackbar.showSnackbar(
                "Oops! You haven't updated detais with us. Kindly fill in details for tour services."
              );
              return CustomerProfileActions.fetchCustomerDetailsFailure({
                error: customer
              });
            }
          }),
          catchError(error => {
            this.snackbar.showSnackbar("Error : Unable to Fetch User Details.");
            return of(CustomerProfileActions.createCustomerFailure({ error }));
          })
        );
      })
    );
  });
}
