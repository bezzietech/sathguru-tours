import { createAction, props } from "@ngrx/store";
import { ICreateUserRequest } from "src/interfaces";

export const createCustomer = createAction(
  "[CUSTOMER PROFILE] CREATE CUSTOMER",
  props<{ newCustomer: ICreateUserRequest }>()
);

export const createCustomerSuccess = createAction(
  "[CUSTOMER PROFILE] CREATE CUSTOMER SUCCESS",
  props<{ newCustomer: any }>()
);

export const createCustomerFailure = createAction(
  "[CUSTOMER PROFILE] CREATE CUSTOMER FAILURE",
  props<{ error: any }>()
);
export const fetchCustomerDetails = createAction(
  "[CUSTOMER PROFILE] FETCH CUSTOMER DETAILS",
  props<{ uid: string }>()
);
export const fetchCustomerDetailsSuccess = createAction(
  "[CUSTOMER PROFILE] FETCH CUSTOMER DETAILS SUCCESS",
  props<{ customer: ICreateUserRequest }>()
);
export const fetchCustomerDetailsFailure = createAction(
  "[CUSTOMER PROFILE] FETCH CUSTOMER DETAILS FAILURE",
  props<{ error: any }>()
);
