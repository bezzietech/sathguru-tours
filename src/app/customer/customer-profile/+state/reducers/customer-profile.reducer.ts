import { createReducer, on } from "@ngrx/store";
import * as CustomerActionTypes from "../actions";
import { ICreateUserRequest } from "src/interfaces";
export const CUSTOMER_FEATURE_KEY = "CUSTOMER_PROFILE";

export interface CustomerProfileState {
  user: any;
}

export interface CustomerProfilePartialState {
  readonly [CUSTOMER_FEATURE_KEY]: CustomerProfileState;
}

export const CustomerProfileInitialState: CustomerProfileState = {
  user: null
};

export const customerProfileReducer = createReducer(
  CustomerProfileInitialState,
  on(CustomerActionTypes.createCustomerSuccess, (state, { newCustomer }) => {
    return { ...state, user: newCustomer };
  }),
  on(CustomerActionTypes.fetchCustomerDetailsSuccess, (state, { customer }) => {
    return { ...state, user: customer };
  })
);

export function customerProfileReducers(state, action) {
  return customerProfileReducer(state, action);
}
