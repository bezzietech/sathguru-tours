
import { createSelector } from '@ngrx/store';
import { AppState } from '../../../../store/app.reducer'
import { CustomerProfileState } from '../reducers'
const getUserState = (state:AppState) => state.customerProfileState

const getUser = createSelector(
    getUserState,
    (state: CustomerProfileState) => state.user
)

export const getUserQuery = {
    getUser
}