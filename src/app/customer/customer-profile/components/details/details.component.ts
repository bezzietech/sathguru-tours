import { Component, OnInit, OnDestroy } from "@angular/core";
import { Observable, Subscription } from "rxjs";
import { ICreateUserRequest, IPost } from "src/interfaces";
import { Store } from "@ngrx/store";
import { AppState } from "src/app/store/app.reducer";
import { getUserQuery } from "../../+state/selectors";
import { MatDialog } from "@angular/material/dialog";
import { EditProfileComponent } from "src/app/edit-profile/edit-profile.component";
import { getPostsQuery } from "../../../customer-feeds/+state/selectors";
import * as jwtDecode from "jwt-decode";
import * as CustomerProfileActions from "../../../customer-profile/+state/actions";
import { environment } from "../../../../../environments/environment";
import * as CustomerFeedsActionTypes from "../../../customer-feeds/+state/actions";
import { Router } from '@angular/router';
@Component({
  selector: "app-customer-details",
  templateUrl: "./details.component.html",
  styleUrls: ["./details.component.scss"],
})
export class DetailsComponent implements OnInit, OnDestroy {
  user$: Observable<ICreateUserRequest>;
  postsList$: Subscription;
  posts: any;
  constructor(public store: Store<AppState>, public dialog: MatDialog, private router: Router) { }
  subscription: Subscription = new Subscription();
  user: any = jwtDecode(localStorage.getItem("access-token"));

  ngOnInit() {
    this.user$ = this.store.select(getUserQuery.getUser);
    this.user$.subscribe((data) => {
      if (data) {
        return;
      } else {
        this.store.dispatch(
          CustomerProfileActions.fetchCustomerDetails({
            uid: this.user.user_id,
          })
        );
      }
    });
    this.postsList$ = this.store
      .select(getPostsQuery.getPosts)
      .subscribe((data) => {
        console.log("data", data);
        if (!data) {
          this.store.dispatch(
            CustomerFeedsActionTypes.fetchPosts({
              path: `${environment.getUserRelatedPostsBaseURL}"${this.user.email}"`,
            })
          );
          return;
        } else {
          this.posts = data;
          this.subscription.add(this.postsList$);
        }
      });
  }
  ngOnDestroy(): void {
    // Called once, before the instance is destroyed.
    // Add 'implements OnDestroy' to the class.
    this.subscription.unsubscribe();
  }

  editProfile() {
    const dialogRef = this.dialog.open(EditProfileComponent, {
      data: this.user$,
    });
    dialogRef.afterClosed().subscribe((result) => {
      console.log("The dialog was closed ", result);
    });
  }

  navigate(route) {
    this.router.navigate([route]);
  }
}
