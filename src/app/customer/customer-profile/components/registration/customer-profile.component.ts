import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Store } from "@ngrx/store";
import { AppState } from "src/app/store/app.reducer";
import * as CustomerProfileActions from "../../+state/actions";
import { Observable } from "rxjs";
import * as jwtDecode from "jwt-decode";
import { ICreateUserRequest } from "src/interfaces";
import { getUserQuery } from "../../+state/selectors";

@Component({
  selector: "app-customer-profile-registration",
  templateUrl: "./customer-profile.component.html",
  styleUrls: ["./customer-profile.component.scss"]
})
export class CustomerProfileComponent implements OnInit {
  formGroup: FormGroup;
  uid: string = jwtDecode(localStorage.getItem("access-token")).user_id;
  address = "";
  userProfile: Observable<any>;
  blood_group = "";
  email_address = "";
  phone_number = "";
  user_name = "";
  profile_photo = "";
  user$: Observable<ICreateUserRequest>;
  constructor(public store: Store<AppState>) {}

  ngOnInit() {
    this.email_address = jwtDecode(localStorage.getItem("access-token")).email;
    this.formGroup = new FormGroup(
      {
        nameGroup: new FormGroup({
          user_name: new FormControl("", Validators.required)
        }),
        profilePhotoGroup: new FormGroup({
          user_photo: new FormControl("", Validators.required)
        }),
        emailGroup: new FormGroup({
          email_address: new FormControl(
            this.email_address,
            Validators.required
          )
        }),
        bloodGroup: new FormGroup({
          blood_group: new FormControl("", Validators.required)
        }),
        phoneNumberGroup: new FormGroup({
          phone_number: new FormControl("", Validators.required)
        }),
        addressGroup: new FormGroup({
          address: new FormControl("", Validators.required)
        })
      },
      Validators.required
    );
    this.user$ = this.store.select(getUserQuery.getUser);
  }

  updateName($event) {
    this.user_name = $event.value;
  }

  updateNumber($event) {
    this.phone_number = $event.value;
  }
  updateAddress($event) {
    this.address = $event.value;
  }
  updateBloodGroup($event) {
    this.blood_group = $event.value;
  }
  updatePhoto($event) {
    $event.click();
  }
  setUserPhoto($event) {
    var filesSelected = $event.files;
    if (filesSelected.length > 0) {
      var fileToLoad = filesSelected[0];
      var fileReader = new FileReader();
      fileReader.onload = (fileLoadedEvent: any) => {
        var srcData = fileLoadedEvent.target.result.toString(); // <--- data: base64
        this.profile_photo = srcData;
      };
    }
  }
  setClass() {
    document.getElementById("#userPhoto").className = "error-image";
  }
  formatDataForSaving(): any {
    let form_values = [];
    Object.entries(this.formGroup.controls).forEach(ele => {
      form_values.push(ele[1].value);
    });
    form_values = form_values.reduce((acc, x) => {
      for (let key in x) acc[key] = x[key];
      return acc;
    }, {});
    form_values["user_photo"] = this.profile_photo;
    return form_values;
  }
  onSubmit() {
    this.updateNumber(
      this.formGroup.controls["phoneNumberGroup"].value["phone_number"]
    );
    const newUser = this.formatDataForSaving();
    this.store.dispatch(
      CustomerProfileActions.createCustomer({ newCustomer: newUser })
    );
  }
}
