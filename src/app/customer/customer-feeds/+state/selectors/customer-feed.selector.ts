import { createSelector } from "@ngrx/store";
import { AppState } from "src/app/store/app.reducer";
import { BlogPostsState } from "../reducers/customer-feed.reducer";

const getPostsState = (state: AppState) => state.blogPostsState;

const getPosts = createSelector(
  getPostsState,
  (state: BlogPostsState) => state.posts
);

export const getPostsQuery = {
  getPosts
};
