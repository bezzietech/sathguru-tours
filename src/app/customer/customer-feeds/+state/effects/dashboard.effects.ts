import { Injectable } from "@angular/core";
import { Actions, ofType, createEffect } from "@ngrx/effects";
import { SnackbarService } from "src/common/services";
import { Router } from "@angular/router";
import * as BlogPostActionTypes from "../actions";
import { switchMap, map, catchError, tap } from "rxjs/operators";
import { of } from "rxjs";
import { Store } from "@ngrx/store";
import { AppState } from "src/app/store/app.reducer";
import { CustomerService } from "../../../customer.service";
@Injectable()
export class BlogPostEffects {
  constructor(
    private snackbar: SnackbarService,
    private actions: Actions,
    private customerService: CustomerService,
    private router: Router,
    private store: Store<AppState>
  ) {}
  postKey = "";

  logAdd = createEffect(() => {
    return this.actions.pipe(
      ofType(BlogPostActionTypes.fetchPosts),
      switchMap(({ path }) => {
        return this.customerService.fetchPosts(path).pipe(
          tap(res => {}),
          map(posts => BlogPostActionTypes.fetchPostsSuccess({ posts })),
          catchError(error => {
            console.log(error);
            this.snackbar.showSnackbar(
              `Unable to fetch posts currently. Please try after some time.`
            );
            return of(BlogPostActionTypes.fetchPostsFailure({ error }));
          })
        );
      })
    );
  });

  createPost = createEffect(() => {
    return this.actions.pipe(
      ofType(BlogPostActionTypes.createPost),
      switchMap(({ post }) => {
        return this.customerService.addPost(post).pipe(
          map(posts => {
            return BlogPostActionTypes.createPostSuccess({ post });
          }),
          tap(res => res),
          catchError(error => {
            this.snackbar.showSnackbar(
              `Unable to fetch posts currently. Please try after some time.`
            );
            return of(BlogPostActionTypes.createPostFailure({ post }));
          })
        );
      })
    );
  });

  hitLikeButton = createEffect(() => {
    return this.actions.pipe(
      ofType(BlogPostActionTypes.likePost),
      switchMap(({ post }) => {
        this.postKey = post["key"];
        return this.customerService.likePost(post).pipe(
          map(post => {
            this.snackbar.showSnackbar("Liked successfully !");
            return BlogPostActionTypes.likePostSuccess({
              post,
              key: this.postKey
            });
          }),
          tap(res => res),
          catchError(error => {
            this.snackbar.showSnackbar(
              "Unable to like post currently. Please try again later"
            );
            return of(BlogPostActionTypes.likePostFailure({ error }));
          })
        );
      })
    );
  });

  hitUnLikeButton = createEffect(() => {
    return this.actions.pipe(
      ofType(BlogPostActionTypes.unlikePost),
      switchMap(({ post }) => {
        this.postKey = post["key"];
        return this.customerService.unlikePost(post).pipe(
          map(post => {
            this.snackbar.showSnackbar("Uniked successfully !");
            return BlogPostActionTypes.unlikePostSuccess({
              post,
              key: this.postKey
            });
          }),
          tap(res => res),
          catchError(error => {
            this.snackbar.showSnackbar(
              "Unable to unlike post currently. Please try again later"
            );
            return of(BlogPostActionTypes.unlikePostFailure({ error }));
          })
        );
      })
    );
  });
}
