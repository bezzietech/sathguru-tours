import { createAction, props } from "@ngrx/store";
import { IPost } from "src/interfaces";

export const fetchPosts = createAction(
  "[POSTS] FETCH POSTS",
  props<{ path: string }>()
);

export const fetchPostsSuccess = createAction(
  "[POSTS] FETCH POSTS SUCCESS",
  props<{ posts: IPost[] }>()
);
export const fetchPostsFailure = createAction(
  "[POSTS] FETCH POSTS FAILURE",
  props<{ error: any }>()
);
export const createPost = createAction(
  "[POSTS] CREATE POST",
  props<{ post: IPost }>()
);
export const createPostSuccess = createAction(
  "[POSTS] CREATE POST SUCCESS",
  props<{ post: any }>()
);
export const createPostFailure = createAction(
  "[POSTS] CREATE POST FAILURE",
  props<{ post: any }>()
);

export const likePost = createAction(
  "[POSTS] HIT LIKE BUTTON",
  props<{ post: IPost }>()
);

export const likePostSuccess = createAction(
  "[POST] HIT LIKE BUTTON SUCCESS",
  props<{ post: IPost; key: string }>()
);

export const likePostFailure = createAction(
  "[POST] HIT LIKE BUTTON FAILURE",
  props<{ error: any }>()
);

export const unlikePost = createAction(
  "[POSTS] HIT UNLIKE BUTTON",
  props<{ post: IPost }>()
);

export const unlikePostSuccess = createAction(
  "[POST] HIT UNLIKE BUTTON SUCCESS",
  props<{ post: IPost; key: string }>()
);

export const unlikePostFailure = createAction(
  "[POST] HIT UNLIKE BUTTON FAILURE",
  props<{ error: any }>()
);
