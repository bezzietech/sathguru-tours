import * as BlogPostsAction from "../actions/customer-feed.actions";
import { createReducer, on } from "@ngrx/store";
import { IPost } from "src/interfaces";
export const BLOG_POST_FEATURE_KEY = "POSTS";

export interface BlogPostsState {
  posts: IPost[];
}

export const blogPostInitialState: BlogPostsState = {
  posts: []
};
export interface BlogPostPartialState {
  readonly [BLOG_POST_FEATURE_KEY]: BlogPostsState;
}
export const blogPostsReducer = createReducer(
  blogPostInitialState,

  on(BlogPostsAction.fetchPostsSuccess, (state, { posts }) => ({
    ...state,
    posts
  })),

  on(BlogPostsAction.createPostSuccess, (state, { post }) => {
    return {
      ...state,
      posts: [...state.posts, post]
    };
  }),

  on(BlogPostsAction.likePostSuccess, (state, { post, key }) => {
    const posts = Object.assign({}, state.posts);
    posts[key] = post;
    return {
      ...state,
      posts
    };
  }),

  on(BlogPostsAction.unlikePostSuccess, (state, { post, key }) => {
    const posts = Object.assign({}, state.posts);
    posts[key] = post;
    return {
      ...state,
      posts
    };
  })
);

export function blogPostsReducers(state, action) {
  return blogPostsReducer(state, action);
}
