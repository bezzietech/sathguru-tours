import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/app.reducer';
import * as CustomerFeedsActionTypes from '../../+state/actions';
import { environment } from 'src/environments/environment';
import { Observable, Subscription } from 'rxjs';
import { getPostsQuery } from '../../+state/selectors';
import { IPost, ICreateUserRequest } from 'src/interfaces';
import { Router, ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import * as CustomerProfileActions from '../../../customer-profile/+state/actions';
import * as jwtDecode from 'jwt-decode';
import { getUserQuery } from '../../../customer-profile/+state/selectors';
import { CreateFeedComponent } from '../create-feed/create-feed.component';
import { MatDialog } from '@angular/material/dialog';
@Component({
  selector: 'app-customer-feeds',
  templateUrl: './customer-feeds.component.html',
  styleUrls: ['./customer-feeds.component.scss']
})
export class CustomerFeedsComponent implements OnInit {
  posts: any;
  postsList$: Observable<any>;
  user$: Observable<ICreateUserRequest>;
  activatedRoute: ActivatedRoute;

  constructor(
    public store: Store<AppState>,
    public sanitizer: DomSanitizer,
    public router: Router,
    public dialog: MatDialog
  ) { }

  uid: string = jwtDecode(localStorage.getItem('access-token')).user_id;
  ngOnInit() {
    this.store.dispatch(
      CustomerProfileActions.fetchCustomerDetails({
        uid: this.uid
      })
    );
    this.store.dispatch(
      CustomerFeedsActionTypes.fetchPosts({
        path: environment.getCustomerPostDetails
      })
    );
    this.postsList$ = this.store.select(getPostsQuery.getPosts);
    this.user$ = this.store.select(getUserQuery.getUser);
  }

  navigate(route) {
    this.router.navigate([route], { relativeTo: this.activatedRoute });
  }

  photoURL(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(CreateFeedComponent, {
      width: '90%',
      panelClass: 'my-dialog-container'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    });
  }

  likeButtonClicked = (post: any) => {
    this.store.dispatch(CustomerFeedsActionTypes.likePost({ post }));
  }
  unlikeButtonClicked = (post: any) => {
    this.store.dispatch(CustomerFeedsActionTypes.unlikePost({ post }));
  }
  checkIfPostLiked(post: IPost) {
    if (!post.hasOwnProperty('likes')) {
      return true;
    }
    if (post.likes.includes(this.uid)) {
      return false;
    } else {
      return true;
    }
  }

  openPrivacy() {
    const dialogRef = this.dialog.open(PrivacyPolicyDialogue);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  openTerms() {
    const dialogRef = this.dialog.open(TermsConditionDialogue);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
}

@Component({
  selector: 'app-privacy-policy',
  templateUrl: '../privacy-policy.dialogue.html',
})
export class PrivacyPolicyDialogue { }

@Component({
  selector: 'app-privacy-policy',
  templateUrl: '../terms-conditions.dialogue.html',
})
export class TermsConditionDialogue { }
