import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerFeedsComponent } from './customer-feeds.component';

describe('CustomerFeedsComponent', () => {
  let component: CustomerFeedsComponent;
  let fixture: ComponentFixture<CustomerFeedsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerFeedsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerFeedsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
