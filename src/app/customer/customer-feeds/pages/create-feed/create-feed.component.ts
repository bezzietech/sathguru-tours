import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/app.reducer';
import * as jwtDecode from 'jwt-decode';
import * as BlogPostActionTypes from '../../+state/actions';
import { IPost } from 'src/interfaces';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-create-feed',
  templateUrl: './create-feed.component.html',
  styleUrls: ['./create-feed.component.scss']
})
export class CreateFeedComponent implements OnInit {
  imgURL;
  vidURL;


  createProjectForm: FormGroup;
  isVideo = false;
  post: IPost = {
    imageUrl: {},
    mDate: '',
    mEmail: '',
    mName: '',
    mSomething: ''
  };
  mName = '';
  mEmail = '';
  file = {};
  constructor(public store: Store<AppState>,
    public dialogRef: MatDialogRef<CreateFeedComponent>) { }

  ngOnInit() {
    this.mName = jwtDecode(localStorage.getItem('access-token')).email.split('@')[0];

    this.mEmail = jwtDecode(localStorage.getItem('access-token')).email;
    this.createProjectForm = new FormGroup({
      mName: new FormControl(this.mName || '', Validators.required),
      mEmail: new FormControl(this.mEmail || '', [
        Validators.required,
        Validators.email
      ]),
      imageUrl: new FormControl(null, Validators.required),
      mSomething: new FormControl('')
    });
  }

  uploadFeedPhoto($event) {
    const filesSelected = $event.files;

    this.file = filesSelected[0];
    const mimeType = filesSelected[0].type;
    const reader = new FileReader();
    if (mimeType.match(/image\/*/) != null) {
      this.vidURL = null;
      reader.readAsDataURL(filesSelected[0]);
      reader.onload = (event) => {
        this.imgURL = reader.result;
      };
    }
    if (mimeType.match(/video\/*/) != null) {
      this.imgURL = null;
      reader.readAsDataURL(filesSelected[0]);
      reader.onload = (event) => {
        this.vidURL = reader.result;
      };
    }

  }

  onSubmitButtonClicked($event) {
    const dt = new Date();
    Object.keys($event.value).forEach(value => {
      this.post[value] = $event.value[value];
    });
    // tslint:disable-next-line: no-string-literal
    this.post['mDate'] = `${(dt.getMonth() + 1)
      .toString()
      .padStart(2, '0')}-${dt
        .getDate()
        .toString()
        .padStart(2, '0')}-${dt
          .getFullYear()
          .toString()
          .padStart(4, '0')} ${dt
            .getHours()
            .toString()
            .padStart(2, '0')}:${dt
              .getMinutes()
              .toString()
              .padStart(2, '0')}:${dt
                .getSeconds()
                .toString()
                .padStart(2, '0')}`;
    // tslint:disable-next-line: no-string-literal
    this.post['flagpos'] = '0';
    this.post.imageUrl = this.file;
    this.store.dispatch(BlogPostActionTypes.createPost({ post: this.post }));
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
