import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "./../../environments/environment";
import { ICreateUserRequest, IPost, ITour } from "./../../interfaces";
import * as firebase from "firebase";
import { tap } from "rxjs/operators";
import * as jwtDecode from "jwt-decode";
import { of } from "rxjs";
import { SnackbarService } from "src/common/services";
import { Router } from "@angular/router";
import * as _ from "lodash";
@Injectable({
  providedIn: "root",
})
export class CustomerService {
  constructor(
    public http: HttpClient,
    public snackBarService: SnackbarService,
    public router: Router
  ) {}
  public headers = new HttpHeaders({
    "Content-Type": "application/json; charset=utf-8",
  });

  uid = jwtDecode(localStorage.getItem("access-token")).user_id;
  addCustomer(newCustomer: ICreateUserRequest) {
    return this.http
      .put(
        `${environment.saveCustomerDatabaseURL}/${this.uid}.json`,
        newCustomer,
        {
          headers: this.headers,
        }
      )
      .pipe(tap((res) => res));
  }

  fetchPosts = (path: string) => {
    return this.http
      .get(path, {
        headers: this.headers,
      })
      .pipe(tap((res: IPost[]) => res));
  };

  addPost = (post$: IPost) => {
    const post = Object.assign({}, post$);
    const fileToUpload = (post.imageUrl as unknown) as File;
    const storageRef = firebase.storage().ref("uploads/" + fileToUpload.name);
    return of(
      storageRef
        .put(fileToUpload)
        .then(async (snapshot) => {
          const posts: any = {};
          post["imageUrl"] = await snapshot.ref.getDownloadURL();
          const uid = firebase.database().ref().child("uploads").push().key;
          posts["/uploads/" + uid] = post;
          await firebase.database().ref().update(posts);
          this.router.navigate(["../customer/sai-feeds"]);
        })
        .catch((error) => {
          this.snackBarService.showSnackbar(error);
        })
    );
  };

  fetchUserDetails = (uid: string) =>
    this.http
      .get(`https://sathguru-d1712.firebaseio.com/id_card_details/${uid}.json`)
      .pipe(tap((res: ICreateUserRequest) => res));

  likePost = (postObject: any) => {
    const post_to_like: IPost = postObject.value;
    const postKey = postObject.key;
    const post = _.cloneDeep(post_to_like);
    if (!post.hasOwnProperty("likes")) {
      post["likes"] = [this.uid];
    } else {
      post.likes.push(this.uid);
    }
    return this.http
      .put(
        `https://sathguru-d1712.firebaseio.com/uploads/${postKey}.json`,
        post
      )
      .pipe(tap((res: IPost) => res));
  };
  unlikePost = (postObject: any) => {
    const post_to_like: IPost = postObject.value;
    const postKey = postObject.key;
    const post = _.cloneDeep(post_to_like);
    const uid_index = post.likes.indexOf(this.uid);
    if (uid_index > -1) {
      post.likes.splice(uid_index, 1);
    }
    return this.http
      .put(
        `https://sathguru-d1712.firebaseio.com/uploads/${postKey}.json`,
        post
      )
      .pipe(tap((res: IPost) => res));
  };
  fetchTours = () =>
    this.http
      .get("https://sathguru-d1712.firebaseio.com/tours.json")
      .pipe(tap((res: ITour[]) => res));

  enrollForTour = (tourObject: any) => {
    const tour: ITour = Object.assign({}, tourObject.value);
    const tourKey = tourObject.key;
    if (!tour.hasOwnProperty("enrolledUsers")) {
      tour["enrolledUsers"] = [this.uid];
    } else {
      tour["enrolledUsers"] = [...tour.enrolledUsers, this.uid];
    }
    return this.http
      .put(`https://sathguru-d1712.firebaseio.com/tours/${tourKey}.json`, tour)
      .pipe(tap((res: ITour) => res));
  };

  cancelEnrollment = (tourObject: any) => {
    const tour: ITour = Object.assign({}, tourObject.value);
    const tourKey = tourObject.key;
    tour["enrolledUsers"] = tour.enrolledUsers
      .slice()
      .filter((user) => user !== this.uid);
    return this.http
      .put(`https://sathguru-d1712.firebaseio.com/tours/${tourKey}.json`, tour)
      .pipe(tap((res: ITour) => res));
  };
}
