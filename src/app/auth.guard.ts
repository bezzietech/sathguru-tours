import { Injectable } from "@angular/core";
import {
  CanActivate,
  CanActivateChild,
  CanLoad,
  Route,
  UrlSegment,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router
} from "@angular/router";
import { Observable } from "rxjs";
import * as jwtDecode from "jwt-decode";
import { SnackbarService } from "src/common/services";
@Injectable({
  providedIn: "root"
})
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {
  constructor(public router: Router, public snackbarService: SnackbarService) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (localStorage.getItem("access-token")) {
      const decodedJWT = jwtDecode(localStorage.getItem("access-token"));
      const expiryTime = Math.round(new Date().getTime() / 1000);
      if (decodedJWT.exp >= expiryTime) {
        return true;
      } else {
        this.snackbarService.showSnackbar(
          "Session expired : Please login again to continue."
        );
        this.router.navigate(["./auth/login"], {
          queryParams: { returnUrl: state.url }
        });
        return false;
      }
    } else {
      this.snackbarService.showSnackbar(
        "Session expired : Please login again to continue."
      );
      this.router.navigate(["./auth/login"]);
      return false;
    }
  }
  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return true;
  }
  canLoad(
    route: Route,
    segments: UrlSegment[]
  ): Observable<boolean> | Promise<boolean> | boolean {
    return true;
  }
}
