import { BrowserModule } from "@angular/platform-browser";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgModule, InjectionToken } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AuthService } from "./auth/auth.service";
import { AppStoreModule } from "./store/app-store.module";
import { EffectsModule } from "@ngrx/effects";
import { AuthEffects } from "./auth/+state/effects";
import { HttpClientModule } from "@angular/common/http";
import { STEPPER_GLOBAL_OPTIONS } from "@angular/cdk/stepper";
import { SnackbarService } from "src/common/services";
import {
  MatSnackBarModule,
  MatButtonModule,
  MatDialogModule,
  MatInputModule,
} from "@angular/material";
import { MatTooltipModule } from "@angular/material/tooltip";
import { StoreModule, ActionReducerMap } from "@ngrx/store";
import { reducers, AppState } from "./store/app.reducer";
// import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from "src/environments/environment";
import { AuthGuard } from "./auth.guard";
import { DoubtsModalComponent } from "../app/customer/customer-tours/components/doubts-modal/doubts-modal.component";
import {
  NgxUiLoaderModule,
  NgxUiLoaderHttpModule,
  NgxUiLoaderRouterModule,
} from "ngx-ui-loader";
import { EditProfileComponent } from "./edit-profile/edit-profile.component";
import {
  PrivacyPolicyDialogue,
  TermsConditionDialogue,
} from "./customer/customer-feeds/pages/load-feeds/customer-feeds.component";
export const REDUCER_TOKEN = new InjectionToken<ActionReducerMap<AppState>>(
  "root reducer"
);
@NgModule({
  declarations: [
    AppComponent,
    DoubtsModalComponent,
    EditProfileComponent,
    PrivacyPolicyDialogue,
    TermsConditionDialogue,
  ],
  imports: [
    HttpClientModule, // import HttpClientModule
    NgxUiLoaderModule, // import NgxUiLoaderModule
    NgxUiLoaderRouterModule,
    NgxUiLoaderHttpModule.forRoot({ showForeground: true }),
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    AppStoreModule,
    BrowserAnimationsModule,
    StoreModule.forRoot({}),
    // StoreDevtoolsModule.instrument({
    //   maxAge: 25, // Retains last 25 states
    //   logOnly: environment.production // Restrict extension to log-only mode
    // }),
    EffectsModule.forRoot([AuthEffects]),
    HttpClientModule,
    MatSnackBarModule,
    MatButtonModule,
    MatTooltipModule,
    MatDialogModule,
    MatInputModule,
  ],
  providers: [
    AuthService,
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { showError: true },
    },
    SnackbarService,
    AuthGuard,
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    DoubtsModalComponent,
    EditProfileComponent,
    PrivacyPolicyDialogue,
    TermsConditionDialogue,
  ],
})
export class AppModule {}
