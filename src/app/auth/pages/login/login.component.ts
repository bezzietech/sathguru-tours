import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { AuthService } from "../../auth.service";
import { IUser } from "src/interfaces";
import { Store } from "@ngrx/store";
import { AppState } from "src/app/store/app.reducer";
import * as loginRequest from "./../../+state/actions";
@Component({
  selector: "app-auth",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class AuthComponent implements OnInit {
  login: FormGroup;
  constructor(
    public authService?: AuthService,
    public store?: Store<AppState>
  ) {}

  ngOnInit() {
    this.login = new FormGroup({
      email: new FormControl("svvsathyanarayanan@gmail.com", [
        Validators.required,
      ]),
      password: new FormControl("sastra10a17", [Validators.required]),
    });
  }

  async onSubmit(userCred: IUser) {
    userCred.returnSecureToken = true;
    userCred.loader = true;
    this.store.dispatch(
      loginRequest.login({
        auth: userCred,
      })
    );
  }
}
