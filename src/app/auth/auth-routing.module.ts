import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {AuthComponent} from './pages/login/login.component'

const routes:Routes =[
    {
        path:'login',
        component:AuthComponent,
        pathMatch:'full'
    }
];
@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule]
})
export class AuthRoutingModule{ }