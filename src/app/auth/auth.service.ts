import { Injectable } from "@angular/core";
import * as firebase from "firebase";
import { environment } from "./../../environments/environment";
import { IUser } from "src/interfaces";
import {
  IErrorMessage,
  ILoginResponse
} from "src/interfaces/login-response.interface";
import { SendErrorMessageService } from "src/common/services/send-error-message.service";
import { of } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { tap } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  constructor(
    public commonService: SendErrorMessageService,
    public http: HttpClient
  ) {
    firebase.initializeApp(environment.firebaseConfig);
  }

  sendLoginRequest(login: IUser) {
    const headers = new HttpHeaders({
      "Content-Type": "application/json; charset=utf-8"
    });
    return this.http
      .post(environment.authenticationURL, login, {
        headers
      })
      .pipe(tap((res: ILoginResponse) => res));
  }

  changeLoaderState(){
    return of({
      loader:false
    })
  }
}
