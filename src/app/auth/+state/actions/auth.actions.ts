import { createAction, props } from "@ngrx/store";
import { IUser } from "src/interfaces";

export const login = createAction("[AUTH] LOGIN", props<{ auth: IUser }>());

export const loginSuccess = createAction(
  "[AUTH] LOGIN_COMPLETE",
  props<{
    idToken: string;
    email: string;
    refreshToken: string;
    expiresIn: number;
    localId: string;
    loader:boolean;
  }>()
);

export const loginFailure = createAction(
  "[AUTH] LOGIN_FAILURE",
  props<{ error: any }>()
);
