import { Injectable } from "@angular/core";
import { Actions, ofType, createEffect } from "@ngrx/effects";
import { AuthService } from "../../auth.service";
import { SnackbarService } from "src/common/services";
import { Router, ActivatedRoute } from "@angular/router";
import * as AuthActionTypes from "../actions";
import { switchMap, map, catchError, tap } from "rxjs/operators";
import { of } from "rxjs";
import { Store } from "@ngrx/store";
import { AppState } from "src/app/store/app.reducer";

@Injectable()
export class AuthEffects {
  returnUrl: string;
  constructor(
    private snackbar: SnackbarService,
    private actions: Actions,
    private authService: AuthService,
    private router: Router,
    private store: Store<AppState>,
    public route: ActivatedRoute
  ) {
    this.returnUrl =
      this.route.snapshot.queryParams["returnUrl"] || "dashboard/overview";
  }

  logAdd = createEffect(() => {
    return this.actions.pipe(
      ofType(AuthActionTypes.login),
      switchMap(({ auth }) => {
        return this.authService.sendLoginRequest(auth).pipe(
          tap(res => {
            localStorage.setItem("access-token", res.idToken);
            this.router.navigateByUrl(this.returnUrl);
          }),
          map(res => AuthActionTypes.loginSuccess(res)),
          catchError(error => {
            this.snackbar.showSnackbar(
              "Login Error. Invalid Username/Password"
            );
            return of(AuthActionTypes.loginFailure({ error }));
          })
        );
      })
    );
  });
}
