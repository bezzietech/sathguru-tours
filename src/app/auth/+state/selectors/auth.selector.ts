
import { createSelector } from '@ngrx/store';
import { AppState } from '../../../store/app.reducer';
import { AuthState } from '../reducers/auth.reducer';

const getAuthState = (state: AppState) => state.authState 

const getToken = createSelector(
    getAuthState,
    (state: AuthState) => state.refreshToken   
)
const getLoader = createSelector(
    getAuthState,
    (state: AuthState) => state.loader
)
export const getTokenQuery = {
    getToken,
    getLoader
}