import { createReducer, on, Action } from "@ngrx/store";
import * as AuthActionTypes from "../actions";
export const AUTH_FEATURE_KEY = "AUTH";

export interface AuthState {
  idToken: string;
  email: string;
  refreshToken: string;
  expiresIn: number;
  localId: string;
  loader: boolean;
}

export interface AuthPartialState {
  readonly [AUTH_FEATURE_KEY]: AuthState;
}

export const authInitialState: AuthState = {
  idToken: "",
  email: "",
  refreshToken: "",
  expiresIn: 0,
  localId: "",
  loader: true
};

export const authReducer = createReducer(
  authInitialState,
  on(
    AuthActionTypes.loginSuccess,
    (
      state,
      { idToken, email, expiresIn, localId, refreshToken, type, loader }
    ) => ({
      ...state,
      idToken,
      email,
      expiresIn,
      localId,
      refreshToken,
      type,
      loader
    })
  )
);
export function authReducers(state, action) {
  return authReducer(state, action);
}
