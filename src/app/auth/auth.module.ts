import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AuthRoutingModule } from "./auth-routing.module";
import {
  MatCardModule,
  MatInputModule,
  MatButtonModule,
  MatToolbarModule,
} from "@angular/material";
import { ReactiveFormsModule } from "@angular/forms";
import { storeFreeze } from "ngrx-store-freeze";
import { EffectsModule } from "@ngrx/effects";

import { AuthComponent } from "./pages/login/login.component";
import { StoreModule } from "@ngrx/store";
import { authReducer, authInitialState, authReducers } from "./+state/reducers";
import { environment } from "src/environments/environment";
import { AuthEffects } from "./+state/effects/auth.effects";
// import { StoreDevtoolsModule } from "@ngrx/store-devtools";

@NgModule({
  declarations: [AuthComponent],
  imports: [
    AuthRoutingModule,
    MatToolbarModule,
    CommonModule,
    MatCardModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    StoreModule.forFeature("authState", authReducers, {
      initialState: authInitialState,
      metaReducers: !environment.production ? [storeFreeze] : [],
    }),
    EffectsModule.forFeature([AuthEffects]),
    !environment.production ? [] : [],
  ],
})
export class AuthModule {}
