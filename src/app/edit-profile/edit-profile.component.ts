import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ICreateUserRequest } from 'src/interfaces';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {
  nameFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(3)
  ]);
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  phoneFormControl = new FormControl('', [
    Validators.required,
    Validators.maxLength(10),
    Validators.minLength(10),
  ]);
  bloodGroupFormControl = new FormControl('', [
    Validators.required,
  ]);
  addressFormControl = new FormControl('', [
    Validators.required,
  ]);

  name = '';
  email = '';
  mobile = '';
  bloodGroup = '';
  address = '';

  matcher = new MyErrorStateMatcher();
  constructor(
    public dialogRef: MatDialogRef<EditProfileComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ICreateUserRequest) { }

  ngOnInit() {
    this.name = this.data.user_name;
    this.email = this.data.email_address;
    this.mobile = this.data.blood_group;
    this.bloodGroup = this.data.blood_group;
    this.address = this.data.address;
  }

  cancel(): void {
    this.dialogRef.close();
  }

  update() {
    const updatedData: ICreateUserRequest = {
      user_name: this.name,
      user_photo: this.data.user_photo,
      email_address: this.email,
      blood_group: this.bloodGroup,
      phone_number: this.mobile,
      address: this.address
    };
    this.dialogRef.close(updatedData);
  }

}
