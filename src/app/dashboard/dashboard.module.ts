import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { DashboardRoutingModule } from "./dashboard-routing.module";
import { UiComponentsModule } from "src/common/ui-components/src/lib/components/ui-components.module";
import { DashboardComponent } from "./pages/dashboard.component";
import {
  MatCardModule,
  MatToolbarModule,
  MatButtonModule,
} from "@angular/material";
import { SliderModule } from "angular-image-slider";

@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    UiComponentsModule,
    MatCardModule,
    MatToolbarModule,
    MatButtonModule,
    SliderModule,
  ],
})
export class DashboardModule {}
