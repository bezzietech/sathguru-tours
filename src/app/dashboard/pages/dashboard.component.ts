import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/app.reducer';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  imagesUrl: any[] = [];
  activatedRoute: ActivatedRoute;

  constructor(public store: Store<AppState>, private router: Router) { }

  ngOnInit() {
    this.imagesUrl = ['../../../assets/images/1.jpg',
      '../../../assets/images/2.jpg',
      '../../../assets/images/3.jpg',
      '../../../assets/images/4.jpg',
      '../../../assets/images/5.jpg',
      '../../../assets/images/6.jpg',
      '../../../assets/images/7.jpg',
      '../../../assets/images/8.jpg',
      '../../../assets/images/9.jpg',
      '../../../assets/images/10.jpg',
    ];
  }

  navigate(route) {
    this.router.navigate([route], { relativeTo: this.activatedRoute });
  }
}
