export interface IUser {
  email: string;
  password: string;
  returnSecureToken: boolean;
  loader: boolean;
}
export interface ICreateUserRequest {
  user_name: string;
  user_photo: string;
  email_address: string;
  blood_group: string;
  phone_number: string;
  address: string;
}

export interface IPost {
  imageUrl?: any;
  mDate?: string;
  mEmail?: string;
  mName?: string;
  mSomething?: string;
  likes?: Array<string>;
}

export interface ITour {
  boardingPoint: Array<string>;
  costOfTrip: string;
  enrolledUsers: Array<string>;
  from: string;
  to: string;
  fromDate: string;
  toDate: string;
  modeOfTransport: string;
  visitingPlaces: Array<string>;
}
