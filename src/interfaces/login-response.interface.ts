export interface ILoginResponse {
  idToken: string;
  email: string;
  refreshToken: string;
  expiresIn: number;
  localId: string;
  loader:boolean
}
export interface IErrorMessage {
  error?: boolean;
  message?: string;
}
