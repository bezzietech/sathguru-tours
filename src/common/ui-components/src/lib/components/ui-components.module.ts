import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatDialogModule,
  MatButtonModule,
  MatInputModule,
  MatSidenavModule,
  MatToolbarModule,
  MatIconModule,
  MatNavList,
  MatListModule
} from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { RouterModule } from '@angular/router';
import { PostLoaderComponent } from './post-loader/post-loader.component';
import { ImageGridComponent } from './image-grid/image-grid.component';

@NgModule({
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    RouterModule
  ],
  declarations: [HeaderComponent, PostLoaderComponent, ImageGridComponent],
  exports: [HeaderComponent, PostLoaderComponent, ImageGridComponent]
})

export class UiComponentsModule { }
