import { Component, ViewChild, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { MatSidenav } from '@angular/material';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import * as jwtDecode from 'jwt-decode';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  label = '';
  userName = '';
  userPhoto = '';
  email = '';
  constructor(
    private breakpointObserver: BreakpointObserver,
    private router: Router,
    private activeRoute: ActivatedRoute
  ) {
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        map(() => this.router)
      )
      .subscribe(event => {
        this.label = event.url
          .split('/')
          .pop()
          .toUpperCase();
      });
  }
  @ViewChild('sidenav', { static: false }) sidenav: MatSidenav;
  isWeb$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Web)
    .pipe(map(result => result.matches));
  isTablet$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Tablet)
    .pipe(map(result => result.matches));
  isHandSet$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));
  close() {
    this.sidenav.close();
  }

  ngOnInit() {
    const userDetails = jwtDecode(
      localStorage.getItem('access-token')
    );
    console.log(userDetails);
    this.userName = userDetails.name;
    this.email = userDetails.email;
    // this.userPhoto = userDetails.user_photo
  }
  navigate(route) {
    this.router.navigate([route]);
  }
  logout() {
    localStorage.clear();
    this.router.navigate(['/auth/login']);
  }
}
