import { TestBed } from '@angular/core/testing';

import { SendErrorMessageService } from './send-error-message.service';

describe('SendErrorMessageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SendErrorMessageService = TestBed.get(SendErrorMessageService);
    expect(service).toBeTruthy();
  });
});
