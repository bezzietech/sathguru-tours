import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class SendErrorMessageService {
  constructor() {}

  sendErrorMessage(errorMessage: string) {
    return {
      error: true,
      message: errorMessage
    };
  }
}
