import { TestBed } from '@angular/core/testing';

import { SnackbarserviceService } from './snackbarservice.service';

describe('SnackbarserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SnackbarserviceService = TestBed.get(SnackbarserviceService);
    expect(service).toBeTruthy();
  });
});
