import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material";

@Injectable({
  providedIn: "root"
})
export class SnackbarService {
  constructor(private snackBar?: MatSnackBar) {}

  showSnackbar(msg: any) {
    this.snackBar.open(msg, null, {
      duration: 3000
    });
  }
}
