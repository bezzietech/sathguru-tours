// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDdgahxPK1WxrdpmJDmrbEI6yTPTFSNdyg",
    authDomain: "sathguru-d1712.firebaseapp.com",
    databaseURL: "https://sathguru-d1712.firebaseio.com",
    projectId: "sathguru-d1712",
    storageBucket: "sathguru-d1712.appspot.com",
    messagingSenderId: "850764076593",
    appId: "1:850764076593:web:b3013c672d4d6aa5782710",
  },
  authenticationURL: `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyDdgahxPK1WxrdpmJDmrbEI6yTPTFSNdyg`,
  saveCustomerDatabaseURL: `https://sathguru-d1712.firebaseio.com/id_card_details`,
  getCustomerPostDetails: `https://sathguru-d1712.firebaseio.com/uploads.json`,
  getUserRelatedPostsBaseURL:
    'https://sathguru-d1712.firebaseio.com/uploads.json?orderBy="mEmail"&equalTo=',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
