export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "AIzaSyDdgahxPK1WxrdpmJDmrbEI6yTPTFSNdyg",
    authDomain: "sathguru-d1712.firebaseapp.com",
    databaseURL: "https://sathguru-d1712.firebaseio.com",
    projectId: "sathguru-d1712",
    storageBucket: "sathguru-d1712.appspot.com",
    messagingSenderId: "850764076593",
    appId: "1:850764076593:web:b3013c672d4d6aa5782710"
  },
  authenticationURL: `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyDdgahxPK1WxrdpmJDmrbEI6yTPTFSNdyg`,
  saveCustomerDatabaseURL: `https://sathguru-d1712.firebaseio.com/id_card_details`,
  getCustomerPostDetails: `https://sathguru-d1712.firebaseio.com/uploads.json`
};
